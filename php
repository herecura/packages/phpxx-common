#!/usr/bin/env bash
# vim: ft=bash:

PHP_SUFFIX="${PHP_SUFFIX}"
PHP_BIN=""

if [[ -z "$PHP_SUFFIX" ]] && [[ -e /etc/default/php ]]; then
    . /etc/default/php
fi

if [[ -n "$PHP_SUFFIX" ]]; then
    PHP_BIN="/usr/bin/php$PHP_SUFFIX"
fi

if [[ -n "$PHP_BIN" ]] && [[ ! -x "$PHP_BIN" ]]; then
    echo "Invalid PHP_SUFFIX given $PHP_BIN not found" >&2
    exit 1
fi

if [[ -z "$PHP_BIN" ]]; then
    PHP_BIN="$(\
        find /usr/bin -mindepth 1 -maxdepth 1 -executable -iname 'php[0-9]*' \
        | sort -V \
        | tail -n1\
    )"
fi

$PHP_BIN "$@"
