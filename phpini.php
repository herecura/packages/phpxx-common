<?php

if ($argc < 4) {
    echo "give php.ini file and extension + section to extract";
    exit(1);
}

$phpinifile = $argv[1];
$phpextension = $argv[2];
$phpinisection = $argv[3];
$priority = $argv[4] ?? '50';

$zend_extension = [
    'opcache',
];

$phpini = [];

$section = "default";

$fh = fopen($phpinifile, 'r');
if ($fh) {
    while (($line = fgets($fh)) !== false) {
        $firstchar = substr($line, 0, 1);
        if ('[' === $firstchar) {
            $section = trim($line);
        }
        $phpini[$section][] = trim($line);
    }
    fclose($fh);
} else {
    echo "failed to open php.ini";
}

$extension = "extension = ";
if (in_array($phpextension, $zend_extension)) {
    $extension = "zend_extension = ";
}

if ("builtin" !== $phpextension) {
    echo ";priority=" . $priority . PHP_EOL;
    echo $extension . $phpextension . PHP_EOL;
}
if (isset($phpini["[" . $phpinisection . "]"])) {
    if ("builtin" !== $phpextension) {
        echo PHP_EOL;
    }
    echo implode(PHP_EOL, $phpini["[" . $phpinisection . "]"]);
}
